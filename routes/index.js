
/*
 * GET home page.
 */

var messages = require('../messages');
var behanceService, userBehance;

exports.setUserBehance = function(user){
	userBehance = user;
};

exports.setBehanceService = function(behance){
	behanceService = behance;
};

exports.index = function(req, res){
  behanceService.getCollectionsDetailsUser(userBehance, function(result){
		res.render("index", { msg: messages.load(), data: result });
	});
  
};

exports.contact = function(req, res){
  res.render('contact', { msg: messages.load() });
};

