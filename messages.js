exports.load = function(){
	return {
		title: 'MJdelunares - Diseño Gráfico & Editorial',
		menu: {
			home: 'Inicio',
			works: 'Trabajos',
			contact: 'Contacto'
		},
		home : {
			title: 'Diseño',
			subtitle: 'Gráfico & Editorial',
			linkStartWork: 'Comencemos un proyecto'
		},
		contact: {
			contacme: 'Para contactar conmigo',
			mail: 'Email',
			twitter: 'Twitter',
			facebook: 'Facebook'
		},
		user: {
			mail: 'mjdelunares@gmail.com',
			descMail: 'mjdelunares [arroba] gmail.com',
			twitter: 'https://twitter.com/MJdelunares',
			descTwitter: '@MJdelunares',
			facebook: 'https://es-es.facebook.com/mariajose.palominofranco',
			descFacebook: 'María José Palomino Franco'
		}
	};
};