(function(){

	var selectorAllPages = ".wrap-page";
	var styleClassHidden = "hidden";


	routie({
		'index': function() {
    	changePage($(".wrap-page-index"));
    },

    '': function(){
			changePage($(".wrap-page-index"));
  	},

		'works': function() {
    	changePage($(".wrap-page-works"));
    },

    'contact': function() {
    	changePage($(".wrap-page-contact"));
    }

    
	});



	function changePage(jPage){
		if (jPage.hasClass(styleClassHidden)) {
			$(selectorAllPages).addClass(styleClassHidden);
			jPage.removeClass(styleClassHidden);
		}
	}


})();

