var template = Hogan.compile('<div id="project-modules-@{{id}}""><ol class="lst-photos-project">{{#modules}}{{#src}}<li class="item-photo-project"><div class="bg-photo-project"><img src="{{src}}" alt="{{caption}}" /><div class="wrap-caption-foto">{{{caption}}}</div></div></li>{{/src}}{{#embed}}<li class="item-photo-project">{{{embed}}}</li>{{/embed}}{{/modules}}</ol></div>');


/*<div id="project-modules-@{{id}}"">
	<ol>
		{{#modules}}
			{{#src}}
			<li>
				<img src="{{src}}" alt="{{caption}}">
			</li>
			{{/src}}
		{{/modules}}
	</ol>
</div>*/

$(function(){
	var STYLE_CLASS_ACTIVE_ITEM_WORK = "item-work-active";
	var STYLE_CLASS_ACTIVE_DETAIL_WORK = "show-detail-work";

	$(".item-work").each(function(){
		var $this = $(this);
		var jWorkWrapper = $this.find(">.work-wrapper");
		var jWorkClose = jWorkWrapper.find(">.work-wrapper-close");
		$this.click(proxyClickItemWork($this, jWorkWrapper));
		jWorkClose.click(proxyClickItemWorkClose(jWorkClose, $this, jWorkWrapper));

	});


	function proxyClickItemWork(jItemWork, jWorkWrapper) {
		return function(){
			if (!jItemWork.hasClass(STYLE_CLASS_ACTIVE_ITEM_WORK)){
				jWorkWrapper.css(jItemWork.offset());
				jItemWork.addClass(STYLE_CLASS_ACTIVE_ITEM_WORK);
				$(document.body).scrollTop(0);
				$(document.body).addClass(STYLE_CLASS_ACTIVE_DETAIL_WORK);
				jWorkWrapper.animate({top: 0, left: 0, width: '100%', height: '100%'}, function(){
					jWorkWrapper.find(".item-project:first").click();
				});
			}
		}
	}


	function proxyClickItemWorkClose(jWorkClose, jItemWork, jWorkWrapper) {
		return function(){
			var offset = jItemWork.offset();
			$(".detail-project-wrapper").empty();
			$(".item-project.selected").removeClass("selected");
			jWorkWrapper.animate({top: offset.top, 
														left: offset.left, 
														width: jItemWork.width(), 
														height: jItemWork.height()}, 
														{
															complete: function(){
																$(document.body).removeClass(STYLE_CLASS_ACTIVE_DETAIL_WORK);
																jItemWork.removeClass(STYLE_CLASS_ACTIVE_ITEM_WORK);
																jWorkWrapper.css({width: 0, height: 0})
															}
														});
		}
	}

	var loadingHTML = '<ol class="lst-photos-project"><li><div class="loading"></div></li></ol>';
	$(".item-project").click(function(){
		$(".item-project").removeClass("selected");
		var $this = $(this);
		$this.addClass("selected")
		var idProjec = $this.data("id");
		var jWrapDetailProject = $this.parent().parent().find(".detail-project-wrapper");
		jWrapDetailProject.html(loadingHTML);
		$.getJSON('api/projects/' + idProjec, function(data) {
			jWrapDetailProject.html(template.render(data.project));
		});
	});


})