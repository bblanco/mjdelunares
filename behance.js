
var http = require('http'),
_ = require('underscore');


var ENDPOINT_COLLECTIONS_USER = "endPointCollectionsUser";
var ENDPOINT_PROJECTS_COLLECTION = "endPointProjectsCollection";
var ENDPOINT_DETAIL_PROJECT = "endPointDetailProject";
var ENDPOINT_DETAIL_USER = "endPointDetailUser";




var defaultEndpoints = [
	{name: ENDPOINT_COLLECTIONS_USER, path: "/users/<%= user %>/collections"},
	{name: ENDPOINT_PROJECTS_COLLECTION, path: "/collections/<%= idCollection %>/projects"},
	{name: ENDPOINT_DETAIL_PROJECT, path: "/projects/<%= idProject %>"},
	{name: ENDPOINT_DETAIL_USER, path: "/users/<%= user %>"}
];

function BehanceService(){

}

BehanceService.prototype = {
	_configuration: {
		apiKey: null,
		contextPath: "http://www.behance.net/v2",
		endpoints: {}
	},

	setApiKey: function(apiKey){
		this._configuration.apiKey = apiKey;
	},

	setEndpoints: function(configurationEndpoints){
		_.each(configurationEndpoints, function(configEnpoint){
			this.setEndpoint(configEnpoint.name, configEnpoint.path);
		}, this);
	},

	setEndpoint: function(endpointName, endpointPath){
		this._configuration.endpoints[endpointName] = _.template(endpointPath);
	},

	_getEndpointConfiguration: function(endpointName){
		return this._configuration.endpoints[endpointName];
	},

	_sendRequest: function(endpoint, onComplete){
		http.get(endpoint, 
				function(response) {
			  		var content = "";
			  		
			  		response.on("data", function(chunk) {
					    content += chunk;
					});

					response.on("end", function(chunk) {
					    var data = JSON.parse(content);
					    onComplete(data);
					});
				}).on('error', function(e) {
				  console.error(e);
				  throw new BehanceException("unable to sendRequest: " + e.message, e);
				});
	},


	_generateFullPathEndpoint: function(endpointName, params){
		params = params || {};
		var pathTemplate = this._getEndpointConfiguration(endpointName);
		
		return this._configuration.contextPath + pathTemplate(params) + 
			   "?api_key=" + this._configuration.apiKey;
	},


	getCollectionsUser: function(user, callback){
		var endpoint = this._generateFullPathEndpoint( ENDPOINT_COLLECTIONS_USER, 
													   {user: user} );
		this._sendRequest(endpoint, callback);
	},

	getProjectsCollection: function(idCollection, callback) {
		var endpoint = this._generateFullPathEndpoint( ENDPOINT_PROJECTS_COLLECTION, 
													   {idCollection: idCollection} );
		this._sendRequest(endpoint, callback);
	},

	getCollectionsDetailsUser: function(user, callback){

		this.getDetailUser(user, _.bind(function(dataUser){

			var endpoint = this._generateFullPathEndpoint( ENDPOINT_COLLECTIONS_USER, 
													   {user: user} );
			var that = this;
			this._sendRequest(endpoint, function(data){

				var countCollections = data.collections.length;
				var countCollectionsLoaded = 0;
				_.each(data.collections, function(collection, index){
					var idCollection = collection.id;

					this.getProjectsCollection(idCollection, (function(indexCollection){
						return function(detailCollection){
							countCollectionsLoaded = countCollectionsLoaded + 1;
							data.collections[indexCollection].projects = detailCollection.projects || [];
							if (countCollectionsLoaded == countCollections){
								data.user = dataUser.user;
								callback(data);
							}
						}					
					})(index));
				}, that);

			});
		}, this));
	},


	getDetailProject: function(idProject, callback) {
		var endpoint = this._generateFullPathEndpoint( ENDPOINT_DETAIL_PROJECT, 
													   {idProject: idProject} );
		this._sendRequest(endpoint, callback);
	},

	getDetailUser: function(user, callback){
		var endpoint = this._generateFullPathEndpoint( ENDPOINT_DETAIL_USER, 
													   {user: user} );
		this._sendRequest(endpoint, callback);
	}

}; 


BehanceException = function(msg, ex){
	this.msg = msg;
	this.ex = ex;
};

var service = new BehanceService();
service.setEndpoints(defaultEndpoints);

exports = module.exports = {
	setApiKey: _.bind(service.setApiKey, service),
	getCollectionsUser: _.bind(service.getCollectionsUser, service),
	getCollectionsDetailsUser: _.bind(service.getCollectionsDetailsUser, service),
	getDetailProject: _.bind(service.getDetailProject, service)
};