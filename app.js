
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , path = require('path') 
  , stylus = require('stylus')
  , nib = require('nib')
  , behance = require('./behance');


behance.setApiKey(process.env.BEHANCE_API_KEY);

routes.setBehanceService(behance);
routes.setUserBehance(process.env.BEHANCE_USER);


var app = express();

function compile(str, path) {
  return stylus(str)
    .set('filename', path)
    .define('url', stylus.url())
    .use(nib())
}

app.configure(function(){
  app.set('port', process.env.VCAP_APP_PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon(__dirname + '/public/favicon.ico')); 
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(stylus.middleware(
    { src: __dirname + '/public',
      compile: compile
    }
  ));
  app.use(express.static(path.join(__dirname, 'public')));
});

app.configure('development', function(){
  app.use(express.errorHandler());
});

app.get('/index', routes.index);
app.get('/', routes.index);
app.get('/api/projects/:idProject', function(req, res){

  behance.getDetailProject(req.params.idProject, function(result){
    res.send(result);
  });
  
});


http.createServer(app).listen(app.get('port'), function(){
  console.log("Express server listening on port " + app.get('port'));
});
